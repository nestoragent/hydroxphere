# For run

Requirements to machine:
- java 8
- git
- maven

To run test need to execute:  `mvn clean install -DTAGS=test_tag`
ex.: `mvn clean install -DTAGS=firstTest`

where -D - this is how maven send value to the tests

To generate allure report, after execute:
`mvn site:site`

report will be generated into folder /target/site/