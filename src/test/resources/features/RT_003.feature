Feature: Test 3

  @RT
  @RT_003
  Scenario: Test 3
    * User goes to the login page

    #Login with Club Administrator Role
    * User is on page "Authorization"
    * User is logged under the role "blueclub"
    * User is on page "Main"
    * User (can only see clubs within his league) "blueclub"
    * User (press button) "Meet Schedule"
    * User is on page "Meet Schedule Page"
    * User (press button) "Add Swim Meet"
    * User is on page "Create New Meet Page"
    * User (selects event order) "Color Run"
    * User (fills in the fields for creating a new meet) with data
      | Club 1           | Blue Club            |
      | Club 2           | Red Club             |
      | Location         | 2                    |
      | Location Name    | Test2                |
      | Time             | 5:30am               |
      | Sort Heats       | Fastest to Slowest   |
      | Course           | Short Course Yards   |
      | Number of Lanes  | 6 Lane               |
      | Lane Assignments | Fixed                |
    * User (press button) "Save"
    * User is on page "Meet Schedule Page"
    * User (checks created meet)
    * User (select edit meet)
    * User (press button) "Continue"
    * User is on page "Create New Meet Page"
    * User (check the displayed meeting information)
