Feature: First test: Login & Roles

  @RT
  @RT_001
  Scenario: First test: Login & Roles
    * User goes to the login page

    #Login with System Administrator Role
    * User is on page "Authorization"
    * User is logged under the role "systemadmin"
    * User is on page "Main"
    * User (can only see clubs within his league) "systemadmin"
    * User (log out)

    #Login with League Administrator Role
    * User is on page "Authorization"
    * User is logged under the role "leagueadmin"
    * User is on page "Main"
    * User (can only see clubs within his league) "leagueadmin"
    * User (log out)

    #Login with Club Administrator Role
    * User is on page "Authorization"
    * User is logged under the role "clubadmin"
    * User is on page "Main"
    * User (can only see clubs within his league) "clubadmin"
    * User (log out)
    * User is on page "Authorization"