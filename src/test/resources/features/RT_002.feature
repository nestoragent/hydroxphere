Feature: 2nd Test: Leagues and Event Orders

  @RT
  @RT_002
  Scenario: 2nd Test: Leagues and Event Orders
    * User goes to the login page

    #Login with System Administrator Role
    * User is on page "Authorization"
    * User is logged under the role "systemadmin"
    * User is on page "Main"
    * User (can only see clubs within his league) "systemadmin"
    * User (press button) "Manage Leagues"
    * User is on page "Manage Leagues Page"
    * User (press button) "Add new league"
    * User is on page "Add Leagues Page"
    * User (fills in the fields for creating a new league) with data
      | League Name   | TestName9         |
      | Time Zone     | Marquesas Islands |
      | Month         | 2                 |
      | Day           | 4                 |
      | City          | Arizona           |
      | State         | Arizona           |
      | Zip Code      | 112               |
      | Is Public     | Yes               |
      | Treasure Name | Mike              |
    * User (press button) "Save Button"
    * User is on page "Add Leagues Page"
    * User (check the data entered)
    * User (rename league) with data
      | League Name | TestName10 |
    * User (press button) "Save Button"
    * User is on page "Add Leagues Page"
    * User (check the renamed league)
    * User (selects event order) "Primary Event Order"
    * User (creates a certain number of events) "35"
    * User (change the location of the event1 to event2) "4" "9"
    * User (check that the event appears right) "6"
    * User (check that the event appears right) "35"
    * User (check that the event appears right) "1"
    * User (check that the event appears right) "22"
    * User (press button) "Add new Event Order"
    * User (adds a new request to the event with name) "New Event Order"
    * User is on page "Add Leagues Page"
    * User (selects event order) "New Event Order"
    * User (creates a certain number of events) "15"
    * User (change the location of the event1 to event2) "3" "7"
    * User (check that the event appears right) "4"
    * User (check that the event appears right) "12"
    * User (check that the event appears right) "15"
    * User (log out)

