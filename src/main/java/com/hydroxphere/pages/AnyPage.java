package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.Page;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by Nestor on 18.03.2016.
 */
public abstract class AnyPage extends Page {

    @FindBy(xpath = ".//*[@id='userbox']/a/i")
    @ElementTitle("Arrow to the Exit")
    public WebElement iArrowDown;

    @FindBy(css = "a[onclick*='logout-form']")
    @ElementTitle("Logout")
    public WebElement aLogout;

    public void wait_new_handler(String handlerCount) {
        try {
            int repeatCount = 0;
            while (Init.getDriver().getWindowHandles().size() != Integer.parseInt(handlerCount) && repeatCount < 10) {
                sleep(1);
                repeatCount++;
            }
        } catch (Exception e) {
            System.err.println("Error when wait new handler. Message = " + e.getMessage());
        }
    }

    public void switch_to_the_new_page() throws Exception {
        String newHandler = Init.getDriverExtensions().
                findNewWindowHandle((Set<String>) Init.getStash().get("mainWindowHandle"), Init.getTimeOut());
        Init.getDriver().switchTo().window(newHandler);
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    public void close_new_page() throws Exception {
        Init.getDriver().close();
        Init.getDriver().switchTo().window(((Set<String>) Init.getStash().get("mainWindowHandle")).iterator().next());
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    protected WebElement findElementByTextFromList(By list, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.getText().equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    protected WebElement findElementByTextFromList(By list, By inner, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.findElement(inner).getText().
                equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    public void log_out() throws Throwable {
        press_button(iArrowDown);
        press_button(aLogout);
    }

    public String getRandomGender() {
        int i = new Random().nextInt(2);

        if (0 == i) {
            return "Girls";
        } else if (i == 1) {
            return "Boys";
        } else {
            return "Mixed";
        }
    }

    public String getRandomDistance() {
        int i = new Random().nextInt(14);

        ArrayList<Integer> distance = new ArrayList<Integer>();
        distance.add(0, 15);
        distance.add(1, 25);
        distance.add(2, 50);
        distance.add(3, 75);
        distance.add(4, 100);
        distance.add(5, 125);
        distance.add(6, 150);
        distance.add(7, 175);
        distance.add(8, 200);
        distance.add(9, 400);
        distance.add(10, 500);
        distance.add(11, 800);
        distance.add(12, 1000);
        distance.add(13, 1500);
        distance.add(14, 1650);

        return distance.get(i).toString();
    }

    public int getLastEventInSortingList(String xPath) {

        List<WebElement> events = Init.getDriver().findElements(By.xpath(xPath));

        int index = events.size() - 1;

        while (index >= 0) {
            WebElement event = events.get(index);
            if (event.isDisplayed()) {
                return index;
            } else {
                index--;
            }

        }

        return 0;
    }
}
