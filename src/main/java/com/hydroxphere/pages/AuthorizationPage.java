package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Authorization")
public class AuthorizationPage extends AnyPage {

    @FindBy(css = "input[name='email']")
    @ElementTitle("Username")
    public WebElement inputLogin;

    @FindBy(css = "input[name='password']")
    @ElementTitle("Password")
    public WebElement inputPassword;

    @FindBy(css = "button[type='submit']")
    @ElementTitle("Sign In")
    public WebElement buttonSubmit;

    public AuthorizationPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(inputLogin));
    }

    public void authenticated_with_login_and_password(String login, String password) throws Exception {
        fill_field(inputLogin, login);
        fill_field(inputPassword, password);
        press_button(buttonSubmit);
    }
}
