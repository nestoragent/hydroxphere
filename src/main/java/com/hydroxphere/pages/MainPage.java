package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Main")
public class MainPage extends AnyPage {

    @FindBy(xpath = "//*[contains(text(), 'Clubhouse Menu')]")
    @ElementTitle("Clubhouse Menu")
    public WebElement clubhouseMenu;

    @FindBy(xpath = "//a[contains(text(), 'Manage Clubs')]")
    @ElementTitle("Manage Clubs")
    public WebElement manageClub;

    @FindBy(xpath = "//a[contains(text(), 'Manage Admin Users')]")
    @ElementTitle("Manage Admin Users")
    public WebElement manageAdmin;

    @FindBy(xpath = "//a[contains(text(), 'Payments Portal')]")
    @ElementTitle("Payments Portal")
    public WebElement paymentPortal;

    @FindBy(id = "meetScheduleTab")
    @ElementTitle("Meet Schedule")
    public WebElement aMeetSchedule;

    public MainPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(clubhouseMenu));
    }

    public void can_only_see_clubs_within_his_league(String roleName) {
        toAllure("Role name is", "roleName");
        switch (roleName) {
            case "systemadmin":
                Assert.assertNotNull("Not find Color League",
                        Init.getDriver().findElements(By.xpath("//td[contains(text(), 'Color League')]")));
                toAllure("Found League Name", "Color League");
                Assert.assertNotNull("Not find Shape League",
                        Init.getDriver().findElements(By.xpath("//td[contains(text(), 'Shape League')]")));
                toAllure("Found League Name", "Shape League");
                Assert.assertNotNull("Not find roleName - Hydroxphere Admin",
                        Init.getDriver().findElements(By.xpath("//span[contains(text(), 'Hydroxphere Admin')]")));
                toAllure("Role is", "Hydroxphere Admin");
                break;
            case "leagueadmin":
                Assert.assertNotNull("Not find Color League",
                        Init.getDriver().findElements(By.xpath("//td[contains(text(), 'Color League')]")));
                toAllure("Found League Name", "Color League");
                Assert.assertNotNull("Not find roleName - League Admin",
                        Init.getDriver().findElements(By.xpath("//span[contains(text(), 'League Admin')]")));
                toAllure("Found League Name", "Shape League");
                toAllure("Role is", "League Admin");
                break;
            case "clubadmin":
                Assert.assertNotNull("Not find Shape League",
                        Init.getDriver().findElements(By.xpath("//td[contains(text(), 'Shape League')]")));
                Assert.assertNotNull("Not find roleName - League Admin",
                        Init.getDriver().findElements(By.xpath("//span[contains(text(), 'League Admin')]")));
                toAllure("Role is", "League Admin");
                break;
        }
    }

}
