package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Manage Leagues Page")
public class ManageLeaguesPage extends AnyPage {

    @FindBy(xpath = "//a[text()='Add new league']")
    @ElementTitle("Add new league")
    public WebElement aAddNewLeague;

    public ManageLeaguesPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(aAddNewLeague));
    }

}
