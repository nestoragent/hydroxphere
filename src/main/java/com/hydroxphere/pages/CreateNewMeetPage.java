package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Create New Meet Page")
public class CreateNewMeetPage extends AnyPage {

    @FindBy(xpath = "//h2[text()='Create New Meet']")
    @ElementTitle("Create New Meet")
    public WebElement aCreateNewMeet;

    @FindBy(id = "LeagueEventOrder")
    @ElementTitle("League Event Order")
    public WebElement selectLeagueEventOrder;

    @FindBy(xpath = "(//*[@id='teamOptionsSelect'])[1]")
    @ElementTitle("Team Options Select 1")
    public WebElement selectTeamOptionsSelect1;

    @FindBy(xpath = "(//*[@id='teamOptionsSelect'])[2]")
    @ElementTitle("Team Options Select 2")
    public WebElement selectTeamOptionsSelect2;

    @FindBy(id = "meetTime")
    @ElementTitle("Meet Time")
    public WebElement inputMeetTime;

    @FindBy(id = "meetDate")
    @ElementTitle("Meet Date")
    public WebElement inputMeetDate;

    @FindBy(id = "location")
    @ElementTitle("Location")
    public WebElement inputLocation;

    @FindBy(id = "unitofmeasurevalue")
    @ElementTitle("Course")
    public WebElement selectCourse;

    @FindBy(id = "selectedLanes")
    @ElementTitle("Number of Lanes")
    public WebElement selectLanes;

    @FindBy(id = "laneAssignmentTypesSelection")
    @ElementTitle("Lane Assignments")
    public WebElement selectLaneAssignments;

    @FindBy(xpath = ".//button[text()='Save']")
    @ElementTitle("Save")
    public WebElement buttonSave;

    public CreateNewMeetPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(aCreateNewMeet));
    }

    public void selects_event_order(String nameEventOrder) {
        new Select(selectLeagueEventOrder).selectByVisibleText(nameEventOrder);
    }

    public void fills_in_the_fields_for_creating_a_new_meet(Map<String, String> data) throws Exception {
        Assert.assertTrue("Location mast be 1 or 2!", Integer.parseInt(data.get("Location")) == 1
                || Integer.parseInt(data.get("Location")) == 2);
        Init.getStash().put("Club 1", data.get("Club 1"));
        Init.getStash().put("Club 2", data.get("Club 2"));
        Init.getStash().put("Location", data.get("Location"));
        Init.getStash().put("Time", data.get("Time"));
        Init.getStash().put("Sort Heats", data.get("Sort Heats"));
        Init.getStash().put("Course", data.get("Course"));
        Init.getStash().put("Number of Lanes", data.get("Number of Lanes"));
        Init.getStash().put("Lane Assignments", data.get("Lane Assignments"));
        Init.getStash().put("Location Name", data.get("Location Name"));

        new Select(selectTeamOptionsSelect1).selectByVisibleText(data.get("Club 1"));
        new Select(selectTeamOptionsSelect2).selectByVisibleText(data.get("Club 2"));
        Init.getDriver().findElement(By.xpath("(//*[@id='HomeClubImg']/img)[" + data.get("Location") + "]")).click();
        fill_field(inputMeetTime, data.get("Time"));

        LocalDateTime currentTime = LocalDateTime.now();
        currentTime = currentTime.plusDays(new Random().nextInt(30));
        currentTime = currentTime.plusMonths(new Random().nextInt(11));
        currentTime = currentTime.plusYears(new Random().nextInt(83));

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Init.getStash().put("DateFormat2", currentTime.format(formatter2));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        Init.getStash().put("Date", currentTime.format(formatter));
        inputMeetDate.sendKeys(Keys.ENTER);
        fill_field(inputMeetDate, currentTime.format(formatter));
        fill_field(inputLocation, data.get("Location Name"));
        new Select(selectCourse).selectByVisibleText(data.get("Course"));
        new Select(selectLanes).selectByVisibleText(data.get("Number of Lanes"));
        new Select(selectLaneAssignments).selectByVisibleText(data.get("Lane Assignments"));

        String nLines = data.get("Number of Lanes");
        char ch = nLines.charAt(0);
        for (int i = 1; i <= Integer.parseInt(ch + ""); i++) {
            if (!Init.getDriver().findElements(By.xpath("//label[@for='checkboxClub0" + i + "']")).isEmpty()) {
                Init.getDriver().findElement(By.xpath("//label[@for='checkboxClub0" + i + "']")).click();
            }
            if (!Init.getDriver().findElements(By.xpath("//label[@for='checkboxClub1" + (i + 1) + "']")).isEmpty()) {
                Init.getDriver().findElement(By.xpath("//label[@for='checkboxClub1" + (i + 1) + "']")).click();
            }
            i++;
        }

    }

    public void check_the_displayed_meeting_information() throws Exception {

        sleep(3);
        String dateInput = inputMeetDate.getAttribute("value");
        Assert.assertTrue("Dates do not match " + dateInput + " != " + Init.getStash().get("DateFormat2").toString(),
                dateInput.equals(Init.getStash().get("Date").toString()));
        toAllure("Date same:", dateInput + " = " + Init.getStash().get("DateFormat2").toString());
        String meetLocation = inputLocation.getAttribute("value");
        Assert.assertTrue("Location Names do not match " + meetLocation + " != " + Init.getStash().get("Location Name").toString(),
                meetLocation.equals(Init.getStash().get("Location Name").toString()));
        toAllure("Location Names same:", meetLocation + " = " + Init.getStash().get("Location Name").toString());
    }


}
