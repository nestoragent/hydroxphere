package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Add Leagues Page")
public class AddLeaguePage extends AnyPage {

    private final String CUT_OFF_MONTH_XPATH = "//select[@name=' cut_off_month']/option[@value='%s']";

    @FindBy(xpath = "//h2[text()='Add League']")
    @ElementTitle("Add League Title Page")
    public WebElement h2TitlePage;

    @FindBy(css = "input[name='name']")
    @ElementTitle("League Name")
    public WebElement inputLeagueName;

    @FindBy(css = "select[name='TimeZoneCode']")
    @ElementTitle("TimeZoneCode")
    public WebElement selectTimeZoneCode;

    @FindBy(css = "select[name=' cut_off_month']")
    @ElementTitle("Cut Off Month")
    public WebElement selectCutOffMonth;

    @FindBy(css = "select[name=' cut_off_day']")
    @ElementTitle("Cut Off Day")
    public WebElement selectCutOffDay;

    @FindBy(css = "input[name='City']")
    @ElementTitle("City")
    public WebElement inputCity;

    @FindBy(css = "select[name='StateID']")
    @ElementTitle("State ID")
    public WebElement selectStateID;

    @FindBy(css = "input[name='zip_code']")
    @ElementTitle("Zip Code")
    public WebElement inputZipCode;

    @FindBy(css = "select[name=' is_public']")
    @ElementTitle("Is Public League")
    public WebElement selectIsPublicLeague;

    @FindBy(css = "input[name='contact[Treasure][name]']")
    @ElementTitle("Treasure Name")
    public WebElement inputTreasureName;

    @FindBy(css = "input[id='submitButton']")
    @ElementTitle("Save Button")
    public WebElement inputSaveSybmitButton;

    @FindBy(id = "ddlEventOrder")
    @ElementTitle("Event Order")
    public WebElement selectEventOrder;

    @FindBy(css = "a[href='#addLeagueEventModal']")
    @ElementTitle("Add Event")
    public WebElement aAddEvent;

    @FindBy(id = "ddlGender")
    @ElementTitle("Gender")
    public WebElement selectGender;

    @FindBy(id = "ddlAgeGroup")
    @ElementTitle("Age Group")
    public WebElement selectAgeGroup;

    @FindBy(id = "ddlDisatance")
    @ElementTitle("Disatance")
    public WebElement selectDisatance;

    @FindBy(id = "ddlRaceType")
    @ElementTitle("Race Type")
    public WebElement selectRaceType;

    @FindBy(css = "button[onclick='addEventorder();']")
    @ElementTitle("Submit")
    public WebElement buttonSubmit;

    @FindBy(css = "a[href='#addEventModal']")
    @ElementTitle("Add new Event Order")
    public WebElement aAddEventOrder;

    @FindBy(id = "custom_event_order")
    @ElementTitle("Custom Event Order")
    public WebElement inputCustomEventOrder;

    @FindBy(css = "button[onclick='saveCustomEventOrder();']")
    @ElementTitle("Submit Custom")
    public WebElement buttonCustomSubmit;
    ArrayList<Integer> eventQueueInfo = new ArrayList<>();

    public AddLeaguePage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(h2TitlePage));
    }

    public void fills_in_the_fields_for_creating_a_new_league(Map<String, String> data) throws Exception {
        Init.getStash().put("League Name", data.get("League Name"));
        Init.getStash().put("Time Zone", data.get("Time Zone"));
        Init.getStash().put("Month", data.get("Month"));
        Init.getStash().put("Day", data.get("Day"));
        Init.getStash().put("City", data.get("City"));
        Init.getStash().put("State", data.get("State"));
        Init.getStash().put("Zip Code", data.get("Zip Code"));
        Init.getStash().put("Is Public", data.get("Is Public"));
        Init.getStash().put("Treasure Name", data.get("Treasure Name"));

        fill_field(inputLeagueName, data.get("League Name"));
        press_button(selectTimeZoneCode);
        Init.getDriver().findElement(By.xpath("//select[@name='TimeZoneCode']/option[contains(text(), 'Marquesas Islands')]")).click();
        press_button(selectCutOffMonth);
        press_button(Init.getDriver().findElement(By.xpath(String.format(CUT_OFF_MONTH_XPATH, data.get("Month")))));
        press_button(selectCutOffDay);
        Init.getDriver().findElement(By.xpath("//select[@name=' cut_off_day']/option[@value='" + data.get("Day") + "']")).click();
        fill_field(inputCity, data.get("City"));
        press_button(selectStateID);
        Init.getDriver().findElement(By.xpath("//select[@name='StateID']/option[text()='" + data.get("State") + "']")).click();
        fill_field(inputZipCode, data.get("Zip Code"));
        press_button(selectIsPublicLeague);
        Init.getDriver().findElement(By.xpath("//select[@name=' is_public']/option[text()='" + data.get("Is Public") + "']")).click();
        fill_field(inputTreasureName, data.get("Treasure Name"));
    }

    public void check_the_data_entered() throws Exception {
        Assert.assertNotNull("Input with data '" + Init.getStash().get("League Name") + "' not found",
                Init.getDriver().findElement(By.xpath("//input[@value='" + Init.getStash().get("League Name") + "']")));
        toAllure("Found Input with data", Init.getStash().get("League Name").toString());
    }

    public void rename_league(Map<String, String> data) throws Exception {
        Init.getStash().put("League Name", data.get("League Name"));
        fill_field(inputLeagueName, data.get("League Name"));
    }

    public void check_the_renamed_league() throws Exception {
        Assert.assertNotNull("Input with data '" + Init.getStash().get("League Name") + "' not found",
                Init.getDriver().findElement(By.xpath("//input[@value='" + Init.getStash().get("League Name") + "']")));
        toAllure("Found Input with data", Init.getStash().get("League Name").toString());
    }


    public void selects_event_order(String nameEventOrder) throws Exception {
        new Select(selectEventOrder).selectByVisibleText(nameEventOrder);
    }

    public void creates_a_certain_number_of_events(String i) throws Exception {
        Init.getStash().put("cratedEventsNumber", i);
        int eventsNumber = Integer.parseInt(i);
        Assert.assertFalse("The requested amount exceeds the limit! Number of events > 50 ", eventsNumber > 50);
        String gender = null;
        Integer ageGroup = 0;
        String randomDistance = null;
        Integer raceType = 0;
        int eventQueue = 0;

        while (eventsNumber != 0) {

            press_button(aAddEvent);
            press_button(selectGender);
            gender = getRandomGender();
            Init.getDriver().findElement(By.xpath("//select[@id='ddlGender']/option[text()='" + gender + "']")).click();
            press_button(selectAgeGroup);
            ageGroup = new Random().nextInt(61) + 1;
            if (ageGroup > 46 && 55 > ageGroup) {
                ageGroup = ageGroup - 10;
            }
            Init.getDriver().findElement(By.xpath("//select[@id='ddlAgeGroup']/option[@value='" + ageGroup + "']")).click();
            press_button(selectDisatance);
            randomDistance = getRandomDistance();
            Init.getDriver().findElement(By.xpath("//select[@id='ddlDisatance']/option[@value='" + randomDistance + "']")).click();
            press_button(selectRaceType);
            raceType = new Random().nextInt(10) + 1;
            Init.getDriver().findElement(By.xpath("//select[@id='ddlRaceType']/option[@value='" + raceType + "']")).click();
            press_button(buttonSubmit);

            sleep(1);
            WebElement presentEvent = Init.getDriver().findElement(By.xpath("(//ul[@id='sortable']/li)[" + (getLastEventInSortingList("//ul[@id='sortable']/li") + 1) + "]"));
            String idElement = presentEvent.getAttribute("id");
            eventQueueInfo.add(eventQueue, Integer.parseInt(idElement));
            toAllure("Event-" + (eventQueue + 1) + ":", " id = " + eventQueueInfo.get(eventQueue));
            eventQueue++;
            eventsNumber--;
        }
    }

    public void change_the_location_of_the_event1_to_event2(String numberOfEvent, String numberMoveToElement) throws Exception {
        Integer maxNumberOfEvent = Integer.parseInt(Init.getStash().get("cratedEventsNumber").toString());
        Integer intNumberOfEvent = Integer.parseInt(numberOfEvent);
        Integer intNumberMoveToElement = Integer.parseInt(numberMoveToElement);
        Assert.assertFalse("Element moves number must be greater than 1 and not larger than the total number in the list = "
                + maxNumberOfEvent, intNumberOfEvent < 1 || intNumberOfEvent > maxNumberOfEvent);
        Assert.assertFalse("Element number where the number of moves the element must be greater than 1 and not more " +
                "than the total number in the list = " + maxNumberOfEvent,
                intNumberMoveToElement < 1 || intNumberMoveToElement > maxNumberOfEvent);

        if (intNumberOfEvent < intNumberMoveToElement) {
            int temp0 = eventQueueInfo.get(intNumberOfEvent - 1);
            for (int i = intNumberOfEvent - 1; i <= intNumberMoveToElement - 2; i++) {
                eventQueueInfo.set(i, eventQueueInfo.get(i + 1));
            }
            eventQueueInfo.set(intNumberMoveToElement - 1, temp0);
        } else if (intNumberOfEvent > intNumberMoveToElement) {
            int temp0 = eventQueueInfo.get(intNumberOfEvent - 1);
            for (int i = intNumberOfEvent - 1; i >= intNumberMoveToElement + 2; i--) {
                eventQueueInfo.set(i, eventQueueInfo.get(i - 1));
            }
            eventQueueInfo.set(intNumberMoveToElement - 1, temp0);
        }

        for (int i = 0; i <= maxNumberOfEvent - 1; i++) {
            toAllure("Event-" + (i + 1) + ":", " id = " + eventQueueInfo.get(i));
        }

        Actions builder = new Actions(Init.getDriver());
        WebElement multiSelectDropDown = Init.getDriver().findElement(By.id("sortable"));
        List<WebElement> dropdownlists = multiSelectDropDown.findElements(By.tagName("li"));
        builder.clickAndHold(dropdownlists.get(Integer.parseInt(numberOfEvent) - 1))
                .moveToElement(dropdownlists.get(Integer.parseInt(numberMoveToElement) - 1))
                .release().build().perform();
        sleep(1);
    }

    public void check_that_the_event_appears_right(String numberEvent) throws Exception {
        Integer findEvent = Integer.parseInt(numberEvent);
        WebElement presentEvent = Init.getDriver().findElement(By.xpath("(//ul[@id='sortable']/li)[" + numberEvent + "]"));
        Integer idElement = Integer.parseInt(presentEvent.getAttribute("id"));
        Integer eventInStash = eventQueueInfo.get(findEvent - 1);
        Assert.assertFalse("Displaced events do not match!", !(idElement.equals(eventInStash)));

    }

    public void adds_a_new_request_to_the_event_with_name(String name) throws Exception {
        fill_field(inputCustomEventOrder, name);
        press_button(buttonCustomSubmit);
    }
}