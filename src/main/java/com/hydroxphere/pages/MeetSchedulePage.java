package com.hydroxphere.pages;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.pageFactory.ElementTitle;
import com.hydroxphere.lib.pageFactory.PageEntry;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Meet Schedule Page")
public class MeetSchedulePage extends AnyPage {

    @FindBy(id = "clikToAdd")
    @ElementTitle("Add Swim Meet")
    public WebElement aAddSwimMeet;

    @FindBy(css = "button[onclick='GetMeetDetails()']")
    @ElementTitle("Continue")
    public WebElement btnContinue;

    public MeetSchedulePage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(aAddSwimMeet));
    }

    public void checks_created_meet() {
        Assert.assertNotNull("Not find Clubs Name to " + Init.getStash().get("DateFormat2"),
                Init.getDriver().findElements(By.xpath("//td[text()='" + Init.getStash().get("DateFormat2") + "']/../td[@data-title='Clubs']")).size());

        String clubsNames = Init.getDriver().findElement(
                By.xpath("//td[text()='" + Init.getStash().get("Date") + "']/../td[@data-title='Clubs']"))
                .getText();
        String clubsNamesStash = Init.getStash().get("Club 1").toString() + Init.getStash().get("Club 2").toString();
        clubsNames = clubsNames.replaceAll(" ", "");
        clubsNamesStash = clubsNamesStash.replaceAll(" ", "");
        clubsNames = clubsNames.replaceAll("\\n", "");
        clubsNamesStash = clubsNamesStash.replaceAll("\\n", "");
        Assert.assertTrue("Club names do not match the previously entered!", clubsNames.equals(clubsNamesStash));
        toAllure("Names of clubs with the same previously introduced", clubsNamesStash);

        Assert.assertNotNull("Not find Edit Meet to " + Init.getStash().get("DateFormat2"),
                Init.getDriver().findElements(By.xpath("//td[text()='" + Init.getStash().get("DateFormat2") + "']/../td[@data-title='Status']/a[text()='Edit Meet']")).size());
        toAllure("Found Edit Meet", Init.getStash().get("DateFormat2").toString());

    }

    public void select_edit_meet() throws IllegalAccessException {
        press_button(Init.getDriver().findElement(By.xpath("//td[text()='" +
                Init.getStash().get("Date") + "']/../td[@data-title='Status']/a[text()='Edit Meet']")));
    }
}
