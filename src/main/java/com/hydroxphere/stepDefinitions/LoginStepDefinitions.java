package com.hydroxphere.stepDefinitions;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.Props;
import com.hydroxphere.pages.AuthorizationPage;
import cucumber.api.java.en.When;


/**
 * Created by VelichkoAA on 14.01.2016.
 */
public class LoginStepDefinitions {

    @When("User goes to the login page$")
    public void User_goes_to_the_login_page() throws Throwable {
        Init.getDriver().get(Props.get("applications.url"));
        Init.getDriverExtensions().waitForPageToLoad();
    }

    @When("User is logged under the role \"(.*?)\"$")
    public void User_is_authenticated_with_login_and_password(String roleName) throws Throwable {
        ((AuthorizationPage) Init.getPageFactory().getCurrentPage())
                .authenticated_with_login_and_password(Props.get(roleName + ".login"),
                        Props.get(roleName + ".password"));
    }

}
