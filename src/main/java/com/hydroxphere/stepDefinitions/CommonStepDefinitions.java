package com.hydroxphere.stepDefinitions;

import com.hydroxphere.lib.Init;
import com.hydroxphere.lib.Props;
import cucumber.api.DataTable;
import cucumber.api.java.en.When;

import java.util.Map;


/**
 * Created by VelichkoAA on 14.01.2016.
 */
public class CommonStepDefinitions {

    @When("^(I|User) is on page \"(.*?)\"$")
    public void init_current_page(String action, String title) throws Throwable {
        Init.getPageFactory().getPage(title);
    }

    @When("^(I|User) \\((.*?)\\) \"([^\"]*)\"$")
    public void standard_action_1(String who, String action, Object param) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action, param);
    }

    @When("^(I|User) \\((.*?)\\).* \"([^\"]*)\".* \"([^\"]*)\"[^\"]*$")
    public void standard_action_2(String who, String action, String param1, String param2) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action, param1, param2);
    }

    @When("^(I|User) \\((.*?)\\)$")
    public void standard_action_3(String who, String action) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action);
    }

    @When("^(I|User) \\((.*?)\\) with data$")
    public void standard_action_4(String who, String action, DataTable dataTable) throws Throwable {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        Init.getPageFactory().getCurrentPage().takeAction(action, data);
    }

    @When("^(I|User) \\((.*?)\\) \"([^\"]*)\" and input data$")
    public void standard_action_5(String who, String action, String param, DataTable dataTable) throws Throwable {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        Init.getPageFactory().getCurrentPage().takeAction(action, param, data);
    }

    @When("^(I|User) go to the Google.com$")
    public void go_to_google(String who) throws Throwable {
        Init.getDriver().get(Props.get("applications.url"));
        Init.getDriverExtensions().waitForPageToLoad();
    }

    @When("^(I|User) go to the page \"(.*?)\"$")
    public void go_to_google(String who, String page) throws Throwable {
        switch (page) {
            case "google.com":
                Init.getDriver().get(Props.get("applications.url"));
                break;
            case "ya.ru":
                Init.getDriver().get("https://ya.ru");
                break;
        }
        Init.getDriverExtensions().waitForPageToLoad();
    }
}
