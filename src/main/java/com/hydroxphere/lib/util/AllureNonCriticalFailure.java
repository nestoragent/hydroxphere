package com.hydroxphere.lib.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbt-velichko-aa on 29.12.2016.
 */
public class AllureNonCriticalFailure {

    private static final Map<Thread, Throwable> failureMap = new HashMap<>();

    public static void fire (Throwable throwable) {
        failureMap.put(Thread.currentThread(), throwable);
    }

    public static Map<Thread, Throwable> getFailure() {
        return failureMap;
    }

    public static void clearFailure() {
        failureMap.clear();
    }
}