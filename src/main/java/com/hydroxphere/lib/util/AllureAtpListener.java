package com.hydroxphere.lib.util;

import com.hydroxphere.lib.Init;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import ru.yandex.qatools.allure.cucumberjvm.AllureRunListener;

import java.awt.*;
import java.io.IOException;

/**
 * Created by SBT-Velichko-AA on 09.03.2016.
 */
public class AllureAtpListener extends AllureRunListener {

    public static final String suiteLabelsKey = "SuiteLabels";

    public void testFailure(Failure failure) {
        try {
            Init.takeFullScreenshot();
        } catch (AWTException | IOException e) {
            System.err.println("Failed to get full screenshot on test finished. Error message = " + e.getMessage());
        }
        super.testFailure(failure);
    }

    @Override
    public void testFinished(Description description) throws IllegalAccessException {
        try {
            Init.takeFullScreenshot();
            if (AllureNonCriticalFailure.getFailure().containsKey(Thread.currentThread())) {
                super.fireTestCaseFailure(AllureNonCriticalFailure.getFailure().get(Thread.currentThread()));
                AllureNonCriticalFailure.clearFailure();
            }
        } catch (AWTException | IOException e) {
            System.err.println("Failed to get full screenshot on test finished. Error message = " + e.getMessage());
        }
        super.testFinished(description);
    }
}
